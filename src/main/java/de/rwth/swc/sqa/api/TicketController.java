package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Controller
public class TicketController implements TicketsApi {
    Map<Long, Ticket> tickets;

    public TicketController() {
        this.tickets = new HashMap<>();
    }

    /**
     * Reacts on a TicketRequest.
     * It checks whether the fromFrom is not in the past and whether the birthdate is not in the future.
     * Only if the disabled value is set to true, it is set to true in the Ticket response.
     * Similarly, the discountCard and Student is handled.
     *
     *
     * @param ticketRequest TicketRequest
     * @return ResponseEntity<Ticket> with
     * id
     * validFrom
     * birthdate
     * validFor
     * disabled
     * discountCard
     * zone
     * student
     */
    @Override
    public ResponseEntity<Ticket> buyTicket(TicketRequest ticketRequest) {
        if (ticketRequest == null) {
            return new ResponseEntity<>(HttpStatus.valueOf(400));
        }
        Ticket ticket = new Ticket();
        // add ticketRequest properties to ticket and set defaults if needed
        ticket.disabled(ticketRequest.getDisabled() != null && ticketRequest.getDisabled());
        ticket.discountCard(ticketRequest.getDiscountCard() != null && ticketRequest.getDiscountCard());
        ticket.setStudent(ticketRequest.getStudent() != null && ticketRequest.getStudent());
        ticket.setBirthdate(ticketRequest.getBirthdate());
        ticket.setZone(getTicketZoneOrNull(ticketRequest.getZone() != null ? ticketRequest.getZone().getValue():null));
        ticket.setValidFrom(ticketRequest.getValidFrom());
        ticket.setValidFor(getTicketValidForOrNull(ticketRequest.getValidFor().getValue()));

        // check if resulting ticket is valid
        if (checkBirthdate(ticket.getBirthdate(), ticket.getValidFrom())
                && checkTicketDate(ticket.getValidFrom())
                && checkValidZoneValidFor(ticket)
                && isTicketAvailable(ticket)
        ) {
            ticket.setId(this.generateNewTicketId());
            this.tickets.put(ticket.getId(), ticket);
            return new ResponseEntity<>(ticket, HttpStatus.valueOf(201));
        } else {
            return new ResponseEntity<>(HttpStatus.valueOf(400));
        }
    }

    static Ticket.ZoneEnum getTicketZoneOrNull(String zone){
        Ticket.ZoneEnum res = null;
        try{
            res = Ticket.ZoneEnum.fromValue(zone);
        }catch (IllegalArgumentException ignore){
            // ignore
        }
        return res;
    }

    static Ticket.ValidForEnum getTicketValidForOrNull(String validFor){
        Ticket.ValidForEnum res = null;
        try{
            res = Ticket.ValidForEnum.fromValue(validFor);
        }catch (IllegalArgumentException ignore){
            // ignore
        }
        return res;
    }

    /**
     * Check whether zone is given if validFor is 1h
     * @param ticket which is checked
     * @return false if condition does not hold, and true in all other cases
     *         (also where validFor is not 1h)
     */
    private boolean checkValidZoneValidFor(Ticket ticket) {
        return !ticket.getValidFor().getValue().equals("1h") || ticket.getZone() != null;
    }

    /**
     * Generates a new id for a Ticket that is currently not used
     * @return id for a new ticketId
     */
    private Long generateNewTicketId(){
        Long id;
        do{
            id = ThreadLocalRandom.current().nextLong();
        }
        while (this.tickets.containsKey(id));
        return id;
    }
    /**
     * Checks if the birthdate of the given customer is set and a valid date.
     * A date is valid if it can be parsed to a LocalDate and the date does not lie in the future.
     * Additionally, the age of a person for which a ticket was bought should be less than 130 years.
     *
     * @param birthdate String to check for
     * @return If the date is valid return true, otherwise false.
     */
    private boolean checkBirthdate(String birthdate, String referenceDate) {
        return checkBirthdate(parseDate(birthdate), parseDate(referenceDate));
    }

    private boolean checkBirthdate(LocalDate birthdate, LocalDate referenceDate) {
        return birthdate != null && !birthdate.isAfter(LocalDate.now()) && getAge(birthdate, referenceDate)<130;
    }

    /**
     * Parse the given date to a LocalDate. If parsing is not possible return null
     *
     * @param date date string to parse
     * @return parsed Date as LocalDate or null if it can not be parsed
     */
    private LocalDate parseDate(String date) {
        LocalDate parsedDate = null;
        try {
            parsedDate = LocalDate.parse(date);
        } catch (DateTimeParseException ignore) {
            // ignore
        }
        if (parsedDate == null){
            try{
                parsedDate = LocalDateTime.parse(date).toLocalDate();
            }catch (DateTimeParseException ignore){
                // ignore
            }
        }
        return parsedDate;
    }

    /**
     * Checks if the date of a ticket (or ticket validation request) is set and a valid date.
     * @param date string to check
     * @return if it is return true, otherwise false.
     */
    private boolean checkTicketDate(String date){
        return parseDateTime(date) != null;
    }

    /**
     * Tries to parse the given date time. If the string can not be parsed then null is returned
     * @param dateTime string to parse to a LocalDateTime
     * @return parsed LocalDateTime or null if it could not be parsed
     */
    private LocalDateTime parseDateTime(String dateTime){
        LocalDateTime parsedDate = null;
        try{
            parsedDate = LocalDateTime.parse(dateTime);
        }catch (DateTimeParseException ignored){
            // ignore
        }
        return parsedDate;
    }

    /**
     * Checks if the zone of a ticket (or ticket validation request) is set and a valid date.
     * @param zone zone string to check
     * @return if it is return true, otherwise false.
     */
    static boolean checkZone(String zone){
        return getTicketZoneOrNull(zone) != null;
    }

    /**
     * If student status is claimed, it is checked whether the candidate is valid for student status.
     * @param student Student status that is claimed.
     * @param birthdate Birthdate of candidate.
     * @return if it is return true, otherwise false.
     */
    private boolean checkStudent(boolean student, String birthdate, String referenceDate){
        if(student){
            return appliesForStudentPrices(parseDate(birthdate), parseDate(referenceDate));
        }else{
            return true;
        }
    }

    /**
     * Check if a ticket is valid given the required information to do a validation.
     * Should be invalid if one of the ticket properties doesn't match the provided properties.
     * Zone B includes Zone A. Furthermore Zone C includes all Zones.
     * @param body TicketValidationRequest object that needs to validated (required)
     * @return if ticket is valid return code 200, otherwise 403
     */
    @Override
    public ResponseEntity<Void> validateTicket(TicketValidationRequest body) {

        if(body.getDisabled()==null){
            body.disabled(false);
        }
        if(body.getStudent()==null){
            body.student(false);
        }
        ResponseEntity<Void> response;
        if(isTvrValid(body)){
            response =  new ResponseEntity<>(HttpStatus.valueOf(200));
        }else {
            response = new ResponseEntity<>(HttpStatus.valueOf(403));
        }
        return response;
    }

    /**
     * Checks if the values of a given ticket validation request are valid.
     * It is checked:
     * - If there exists a ticket with the given ticketId.
     * - If the discountCardId is set and exists (if the ticket requires a discount card)
     * - If the values of the date and zone are set and valid and if student status matches birthdate
     * - If the properties of the ticket validation request match the properties of the ticket
     * @param tvr TicketValidationRequest object to check
     * @return if check is successful return true, otherwise false
     */
    private boolean isTvrValid(TicketValidationRequest tvr){
        Ticket ticket = tickets.get(tvr.getTicketId());
        return existsTicket(tvr.getTicketId())
                && hasCorrespondingDiscountCard(tvr)
                && checkTicketDate(tvr.getDate())
                && checkZone(tvr.getZone().getValue())
                && checkStudent(tvr.getStudent(),ticket.getBirthdate(), ticket.getValidFrom())
                && checkMatchingProperties(tvr)
                && validateZone(tvr)
                && validateDate(tvr)
                && validateDiscountCard(tvr);
    }

    boolean existsTicket(Long id){
        return this.tickets.containsKey(id);
    }

    boolean hasCorrespondingDiscountCard(TicketValidationRequest tvr){
        Ticket ticket = tickets.get(tvr.getTicketId());
        return !ticket.getDiscountCard() || getDiscountCard(tvr.getDiscountCardId()) != null;
    }

    /**
     * Check whether the properties of the ticket validation request match the properties of the ticket.
     * Also checks whether the properties of the ticket match the properties of the customer (if the ticket requires a discount card)
     * @param ticketValidationRequest TicketValidationRequest object to check
     * @return if properties match return true, otherwise false
     */
    private boolean checkMatchingProperties(TicketValidationRequest ticketValidationRequest){
        Ticket ticket = this.tickets.get(ticketValidationRequest.getTicketId());
        if((ticket.getDisabled() && !ticketValidationRequest.getDisabled())
                || (ticket.getStudent() && !Objects.equals(ticketValidationRequest.getStudent(), ticket.getStudent()))) {
            return false;
        }
        if(ticket.getDiscountCard()){
            Customer customer = getCustomer(getDiscountCard(ticketValidationRequest.getDiscountCardId()).getCustomerId());
            if(!ticket.getBirthdate().equals(customer.getBirthdate())){
                return false;
            }
            if(ticket.getDisabled() && !customer.getDisabled()){
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the ticket is valid in the zone the validator is in
     * @param ticketValidationRequest ticketValidationRequest object to validate the zone
     * @return if zone is valid return true, otherwise false
     */
    private boolean validateZone(TicketValidationRequest ticketValidationRequest){
        Ticket ticket = tickets.get(ticketValidationRequest.getTicketId());
        boolean isValid;
        if (ticket != null && Ticket.ValidForEnum._1H.equals(ticket.getValidFor())){
            isValid = ticket.getZone().getValue().compareTo(ticketValidationRequest.getZone().getValue()) >= 0;
        }else {
            isValid = true;
        }
        return isValid;
    }

    /**
     * Check whether the ticket is valid at the time of the validation request
     * @param ticketValidationRequest ticketValidationRequest object to validate the date
     * @return if date is valid return true, otherwise false
     */
    private boolean validateDate(TicketValidationRequest ticketValidationRequest){
        Ticket ticket = tickets.get(ticketValidationRequest.getTicketId());
        LocalDateTime validFrom = LocalDateTime.parse(ticket.getValidFrom());
        LocalDateTime validUntil = getValidUntil(validFrom, ticket.getValidFor()).plusMinutes(1);  // include the current minute
        LocalDateTime dateTime = LocalDateTime.parse(ticketValidationRequest.getDate());
        return dateTime.isEqual(validFrom)
                || dateTime.isEqual(validUntil)
                || (dateTime.isAfter(validFrom) && dateTime.isBefore(validUntil));
    }

    /**
     * Get the point in time until which a ticket is valid
     * @param validFrom point in time from which the ticket is valid
     * @param validFor duration the ticket is valid for
     * @return point in time until which a ticket is valid
     */
    private LocalDateTime getValidUntil(LocalDateTime validFrom, Ticket.ValidForEnum validFor){
        return switch (validFor){
            case _1H -> validFrom.plusHours(1);
            case _1D -> validFrom.plusDays(1);
            case _30D -> validFrom.plusDays(30);
            case _1Y -> validFrom.plusYears(1);
        };
    }

    /**
     * If discount card is required for the ticket, check if discount card is valid at time of validation
     * @param ticketValidationRequest ticketValidationRequest object to validate the date
     * @return if discount card is not required or discount card is valid return true, otherwise false
     */
    private boolean validateDiscountCard(TicketValidationRequest ticketValidationRequest){
        if(tickets.get(ticketValidationRequest.getTicketId()).getDiscountCard()){
            DiscountCard discountCard = getDiscountCard(ticketValidationRequest.getDiscountCardId());
            LocalDate validFrom = LocalDate.parse(discountCard.getValidFrom());
            LocalDate validUntil;
            if(discountCard.getValidFor() == DiscountCard.ValidForEnum._30D){
                validUntil = validFrom.plusDays(30);
            }else{      //case _1Y
                validUntil = validFrom.plusYears(1);
            }
            LocalDate date = LocalDateTime.parse(ticketValidationRequest.getDate()).toLocalDate();
            return date.isEqual(validFrom)
                    || date.isEqual(validUntil)
                    || (date.isAfter(validFrom) && date.isBefore(validUntil));
        }
        return true;
    }

    DiscountCard getDiscountCard(Long id){
        return  CustomerController.discountCards.get(id);
    }

    Customer getCustomer(Long id){
        return  CustomerController.customers.get(id);
    }

    boolean isSenior(LocalDate birthdate, LocalDate referenceDate){
        return checkBirthdate(birthdate, referenceDate) && getAge(birthdate, referenceDate) >= 60;
    }

    boolean appliesForStudentPrices(LocalDate birthdate, LocalDate referenceDate){
        return checkBirthdate(birthdate, referenceDate) && getAge(birthdate, referenceDate) < 28;
    }

    boolean appliesForChildPrices(LocalDate birthdate, LocalDate referenceDate){
        return checkBirthdate(birthdate, referenceDate) && getAge(birthdate, referenceDate) < 14;
    }

    boolean isTicketAvailable(Ticket ticket){
        LocalDate birthdate = parseDate(ticket.getBirthdate());
        LocalDate referenceDate = parseDate(ticket.getValidFrom());
        boolean isAvailable = true;
        if (appliesForChildPrices(birthdate, referenceDate)){
            isAvailable = !ticket.getValidFor().equals(Ticket.ValidForEnum._1D);
        } else if (ticket.getStudent() && appliesForStudentPrices(birthdate, referenceDate)) {
            isAvailable = List.of(Ticket.ValidForEnum._30D, Ticket.ValidForEnum._1Y).contains(ticket.getValidFor());
        } else if (isSenior(birthdate, referenceDate)) {
            isAvailable = List.of(Ticket.ValidForEnum._30D, Ticket.ValidForEnum._1Y, Ticket.ValidForEnum._1D).contains(ticket.getValidFor());
        }
        return isAvailable;
    }

    int getAge(LocalDate birthdate, LocalDate referenceDate){
        return referenceDate.getYear() - birthdate.getYear();
    }

}
