package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Controller
public class CustomerController implements CustomersApi{

    static Map<Long, Customer> customers = new HashMap<>();
    static Map<Long, DiscountCard> discountCards = new HashMap<>();
    SimpleDateFormat formatter;

    public CustomerController() {
        this.formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setLenient(false);
    }

    @Override
    public ResponseEntity<Customer> addCustomer(Customer body) {
        if (body.getDisabled()==null){
            body.disabled(false);
        }
        if (checkCustomerId(body) && checkBirthdate(body)) {
            // input valid add customer
            customers.put(body.getId(), body);
            return new ResponseEntity<>(body, HttpStatus.valueOf(201));
        }else{
            return new ResponseEntity<>(HttpStatus.valueOf(400));
        }
    }

    /**
     * Generates a new id for a Customer that is currently not used ba any other customer
     * @return id for a new Customer
     */
    private Long generateNewCustomerId(){
        Long id;
        do{
            id = ThreadLocalRandom.current().nextLong();
        }
        while (customers.containsKey(id));
        return id;
    }

    /**
     * Generates a new id for a DiscountCard that is currently not used
     * @return id for a new discountCard
     */
    private Long generateNewDiscountCardId(){
        Long id;
        do{
            id = ThreadLocalRandom.current().nextLong();
        }
        while (discountCards.containsKey(id));
        return id;
    }

    /**
     * Checks if the birthdate of the given customer is set and a valid date.
     * A date is valid if it can be parsed to a LocalDate and the date does not lie in the future.
     *
     * @param customer customer to check the birthday of
     * @return If the date is valid return true, otherwise false.
     */
    private boolean checkBirthdate(Customer customer){
        LocalDate date = parseDate(customer.getBirthdate());
        return date != null && !date.isAfter(LocalDate.now());
    }

    /**
     * Checks if  the id of the given customer is set.
     * If it is set return false if the id already exists.
     * If the id is not set generate and assign a new id.
     * @param customer customer to verify the id
     * @return True if the id of the customer is not set, otherwise false
     */
    private boolean checkCustomerId(Customer customer){
        if (customer.getId() == null){
            customer.id(generateNewCustomerId());
            return true;
        } else return false;

    }

    @Override
    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(Long customerId) {
        List<DiscountCard> customerDiscountCards = this.getDiscountCardsOfCustomer(customerId);
        if (!customerDiscountCards.isEmpty()){
            return new ResponseEntity<>(customerDiscountCards, HttpStatus.valueOf(200));
        }else{
            return new ResponseEntity<>(HttpStatus.valueOf(404));
        }
    }

    @Override
    public ResponseEntity<DiscountCard> addDiscountCardToCustomer(Long customerId, DiscountCard body) {
        if (!customers.containsKey(customerId)){
            return new ResponseEntity<>(HttpStatus.valueOf(404));
        }else if (customerId.equals(body.getCustomerId()) && checkDiscountCardType(body) && checkDate(body.getValidFrom())) {
            if (hasNoOverlappingDiscountCard(body)){
                body.id(this.generateNewDiscountCardId());
                discountCards.put(body.getId(), body);
                return new ResponseEntity<>(body, HttpStatus.valueOf(201));
            }else return new ResponseEntity<>(HttpStatus.valueOf(409));
        }else{
            return new ResponseEntity<>(HttpStatus.valueOf(400));
        }
    }

    /**
     * Checks if for the given {@link DiscountCard} the customer already has a {@link DiscountCard} with an overlapping
     * timeframe
     * @param discountCard DiscountCard for which the timeframe should be checked
     * @return true if the customer currently has no other DiscountCards with an overlapping timeframe, otherwise false.
     */
    boolean hasNoOverlappingDiscountCard(DiscountCard discountCard){
        List<DiscountCard> customerDiscountCards = this.getDiscountCardsOfCustomer(discountCard.getCustomerId());
        LocalDate beginTimeframe = parseDate(discountCard.getValidFrom());
        boolean hasNoOverlap = false;
        if (beginTimeframe != null) {
            LocalDate endTimeframe = getTimeframeEnd(beginTimeframe, discountCard.getValidFor());
            hasNoOverlap = customerDiscountCards.isEmpty() || customerDiscountCards.stream().anyMatch(customerDiscountCard -> {
                LocalDate start = parseDate(customerDiscountCard.getValidFrom());
                LocalDate end = getTimeframeEnd(start, customerDiscountCard.getValidFor());
                return isNotInTimeframe(start, end, beginTimeframe) && isNotInTimeframe(start, end, endTimeframe);
            });
        }
        return hasNoOverlap;

    }

    /**
     * Checks if the given Date can be parsed to a LocalDate
     * @param date date string to parse
     * @return True if given String can be parsed to a date. Otherwise, false
     */
    private boolean checkDate(String date){
        return parseDate(date) != null;
    }

    /**
     * Parse the given date to a LocalDate. If parsing is not possible return null
     * @param date date string to parse
     * @return parsed Date as LocalDate or null if it can not be parsed
     */
    private LocalDate parseDate(String date){
        try {
            return LocalDate.parse(date);
        } catch (DateTimeParseException e) {
            return null;
        }
    }

    /**
     * Checks if the type of the given DiscountCard is valid.
     * The type is valid if the value of the type is in [25,50].
     * @param discountCard DisountCard to verify the type of
     * @return True if the type is valid, otherwise false
     */
    private boolean checkDiscountCardType(DiscountCard discountCard){
        List<Integer> allowedTypes = Arrays.asList(25, 50);
        return allowedTypes.contains(discountCard.getType());
    }

    /**
     * Add the time interval given in validFor to the given date and return the resulting end date
     * @param beginTimeframe start date of the timeframe
     * @param validFor time interval of the timeframe. Must be in ["30d", "1y"]
     * @return end date of the timeframe
     */
    static LocalDate getTimeframeEnd(LocalDate beginTimeframe, DiscountCard.ValidForEnum validFor){
        return switch (validFor){
            case _1Y -> beginTimeframe.plusYears(1);
            case _30D -> beginTimeframe.plusDays(30);
        };
    }

    /**
     * Checks if the given date is between the startDate and endDate.
     *  startDate <= date <= endDate
     * @param startDate start date of the timeframe
     * @param endDate end date of the timeframe
     * @param date date to check if it lies between
     * @return True if the date is within the timeframe, otherwise false
     */
    private boolean isDateBetween(LocalDate startDate, LocalDate endDate, LocalDate date){
        return date.isAfter(startDate) && date.isBefore(endDate);
    }

    /**
     * Checks if the given date is outside the given timeframe
     *  startDate >= date  and date >= endDate
     * @param startDate start date of the timeframe
     * @param endDate end date of the timeframe
     * @param date date to check if it lies outside
     * @return True if the date is outside the timeframe, otherwise false
     */
    private boolean isNotInTimeframe(LocalDate startDate, LocalDate endDate, LocalDate date){
        return !(isDateBetween(startDate, endDate, date));
    }

    /**
     * Get all DiscountCards of a Customer identified be the given id
     * @param customerId id of the customer
     * @return list of all DiscountCards of a customer
     */
    private List<DiscountCard> getDiscountCardsOfCustomer(Long customerId){
        return discountCards.values().stream()
                .filter(discountCard -> discountCard.getCustomerId().equals(customerId))
                .toList();
    }

}
