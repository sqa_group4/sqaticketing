package de.rwth.swc.sqa.api;

import io.restassured.RestAssured;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

import static de.rwth.swc.sqa.api.CustomerControllerTest.ADD_CUSTOMER_PATH;
import static de.rwth.swc.sqa.api.CustomerControllerTest.DISCOUNTCARD_PATH;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class TicketControllerTest {

    private static final String ADD_TICKET_PATH = "/tickets";
    private static final String VALIDATE_TICKET_PATH = ADD_TICKET_PATH + "/validate";
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String CONTENT_TYPE = "application/json";

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
    @LocalServerPort
    private int port;

    @BeforeEach
    void setUp() {
        RestAssured.port = port;
    }


    @ParameterizedTest
    @CsvSource({
            "2022-10-01T08:30:20,1940-01-01, 1d",
            "2022-09-03T23:59:59,2020-12-01, 30d",
            "2022-06-09T23:50:10,1968-03-13, 1y"
    })
    void buyTicket (String validFrom, String birthdate, String validFor) {
        JSONObject ticketRequestData = getTicketRequest(validFrom, birthdate, validFor);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(ticketRequestData.toString())
                .post(ADD_TICKET_PATH)
                .then()
                .statusCode(201)
                .body("validFrom", equalTo(validFrom))
                .body("birthdate", equalTo(birthdate))
                .body("validFor", equalTo(validFor))
                .body("disabled", is(false))
                .body("student", is(false))
                .body("discountCard", is(false));
    }

    @ParameterizedTest(name = "testing for {3}")
    @CsvSource({
            "2021-06-01T10:20:40,1972-01-01, 1h, 1h ticket with zone missing",
            "2021-06-01T21:32:45,2024-04-05, 1y, rejecting unborn customer",
            "2021-06-01T21:32:45,1972-01-01,,validFor null"
    })
    void buyTicket_invalid (String validFrom, String birthdate, String validFor, String testDesc) {
        JSONObject ticketRequestData = getTicketRequest(validFrom, birthdate, validFor);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(ticketRequestData.toString())
                .post(ADD_TICKET_PATH)
                .then()
                .statusCode(400);
    }

    @ParameterizedTest
    @CsvSource({
            ",2022-06-01T09:49:33,1999-01-01, 30d, true,true, C,true",
            "10,2022-09-03T06:20:30,2020-12-01, 1y, false,false, B, ",
            ",2022-06-09T10:11:41,1968-03-13, 30d, false, , , false"
    })
    void buyTicket_extended (String id, String validFrom, String birthdate, String validFor,
                             Boolean disabled, Boolean discountCard, String zone, Boolean student) {
        JSONObject ticketRequestData = getTicketRequest_extended(id, validFrom, birthdate, validFor,String.valueOf(disabled), String.valueOf(discountCard), zone, String.valueOf(student));
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(ticketRequestData.toString())
                .post(ADD_TICKET_PATH)
                .then()
                .statusCode(201)
                .body("id", not(nullValue()))
                .body("validFrom", equalTo(validFrom))
                .body("birthdate", equalTo(birthdate))
                .body("validFor", equalTo(validFor))
                .body("disabled", equalTo(disabled != null && disabled))
                .body("discountCard", equalTo(discountCard != null && discountCard))
                .body("zone", equalTo(zone))
                .body("student", equalTo(student != null && student));
    }


    @ParameterizedTest(name = "testing for {8}")
    @CsvSource({
            ",2022-06-01T05:15:51,1962-12-30, 1h, false,true, , , 1h ticket with zone missing",
            ",2022-06-01T14:37:56,2025-12-30, 1h, ,true, ,true, 1h ticket with zone missing",
            ",2022-06-01T14:37:56,1952-01-31, 1h, ,true, ,true, student ticket for older person",
            ",2022-06-01T14:37:56,1892-01-31, 1h, ,true, ,false, ticket for person with impossible age",
    })
    void buyTicket_extended_invalid (String id, String validFrom, String birthdate, String validFor,
                             Boolean disabled, Boolean discountCard, String zone, Boolean student, String testDesc) {
        JSONObject ticketRequestData = getTicketRequest_extended(id, validFrom, birthdate, validFor,String.valueOf(disabled), String.valueOf(discountCard), zone, String.valueOf(student));
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(ticketRequestData.toString())
                .post(ADD_TICKET_PATH)
                .then()
                .statusCode(400);
    }

    static JSONObject getTicketRequest_extended(String id,String validFrom, String birthdate, String validFor,
                                         String disabled, String discountCard, String zone, String student){
        JSONObject ticketRequestData = new JSONObject();
        try {
            ticketRequestData.put("id", id);
            ticketRequestData.put("validFrom", validFrom);
            ticketRequestData.put("birthdate", birthdate);
            ticketRequestData.put("validFor", validFor);
            ticketRequestData.put("disabled", disabled);
            ticketRequestData.put("discountCard", discountCard);
            ticketRequestData.put("zone", zone);
            ticketRequestData.put("student", student);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return ticketRequestData;
    }
    JSONObject getTicketRequest(String validFrom, String birthdate, String validFor){
        JSONObject ticketRequestData = new JSONObject();
        try {
            ticketRequestData.put("validFrom", validFrom);
            ticketRequestData.put("birthdate", birthdate);
            ticketRequestData.put("validFor", validFor);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return ticketRequestData;
    }


    @ParameterizedTest(name = "testing for {2}")
    @MethodSource("provideInvalidTicketValidationRequests")
    void testTicketValidationWithInvalidInput(JSONObject ticketRecord, JSONObject ticketValidationRecord, String testDesc){
        Long ticketId = addTicket(ticketRecord);
        try {
            ticketValidationRecord.put("ticketId", ticketId);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(ticketValidationRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(403);
    }

    private static Stream<Arguments> provideInvalidTicketValidationRequests() {

        JSONObject ticketRecord = getTicketRequest_extended(null, LocalDateTime.now().minusHours(5).format(formatter), "1980-01-01",  "1d","false", "false", null, "true");
        return Stream.of(
                Arguments.of(
                        ticketRecord,
                        getTicketValidationRequest(null,"C", LocalDateTime.now().toString(), "false", null, "true"),
                        "customer is not a student"),
                Arguments.of(
                        ticketRecord,
                        getTicketValidationRequest(null,"C", LocalDateTime.now().toString(), "false", null, "false"),
                        "customer is not a student")
        );
    }

    @ParameterizedTest
    @CsvSource({
            "1h,A",
            "1h,B",
            "1h,C",
            "1d,"
    })
    void testTicketValidationInvalidStudentTickets(String validFor, String zone){
        JSONObject ticketRecord = getTicketRequest_extended(null, LocalDateTime.now().minusHours(5).format(formatter),
                LocalDate.now().minusYears(25).toString(),  validFor,"false", "false", zone, "true");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(ticketRecord.toString())
                .post(ADD_TICKET_PATH)
                .then()
                .statusCode(400);
    }

    @ParameterizedTest
    @CsvSource({
            //test senior
            "1962-01-01,1h,false, 400",
            "1962-01-01,1d,false, 201",
            "1962-01-01,30d,false, 201",
            "1962-01-01,1y,false, 201",
            //test Adult
            "1982-01-01,1h,false, 201",
            "1982-01-01,1d,false, 201",
            "1982-01-01,30d,false, 201",
            "1982-01-01,1y,false, 201",
            //test Student
            "1995-01-01,1h, true, 400",
            "1995-01-01,1d, true, 400",
            "1995-01-01,30d, true, 201",
            "1995-01-01,1y, true,201",
            //test Child
            "2010-01-01,1h,false, 201",
            "2010-01-01,1d,false, 400",
            "2010-01-01,30d,false, 201",
            "2010-01-01,1y,false, 201",

    })
    void testBuyTicket_TicketAvailable(String birthdate,String validFor, String student, Integer statusCode){
        JSONObject ticketRecord = getTicketRequest_extended(null, LocalDateTime.now().minusHours(5).format(formatter),
                birthdate,  validFor,"false", "false", "A", student);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(ticketRecord.toString())
                .post(ADD_TICKET_PATH)
                .then()
                .statusCode(statusCode);
    }

    @ParameterizedTest
    @CsvSource({
            "1h,A,A,200",
            "1h,B,A,200",
            "1h,C,A,200",
            "1h,A,B,403",
            "1h,B,B,200",
            "1h,C,B,200",
            "1h,A,C,403",
            "1h,B,C,403",
            "1h,C,C,200",
            "1d,A,C,200",
            "30d,A,C,200",
            "1y,A,C,200",
    })
    void testTicketValidationInvalidZone(String validFor, String zoneTicket, String zoneTvr, Integer expetedStatusCode){
        JSONObject ticketRecord = getTicketRequest_extended(null, LocalDateTime.now().minusMinutes(30).format(formatter),
                LocalDate.now().minusYears(35).toString(),  validFor,"false", "false", zoneTicket, "false");
        Long ticketId = addTicket(ticketRecord);
        JSONObject tvrRecord = getTicketValidationRequest(ticketId.toString(), zoneTvr, LocalDateTime.now().toString(), "false", null, "false");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(tvrRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(expetedStatusCode);
    }

    @ParameterizedTest
    @CsvSource({
            "true,true,200",
            "false,false,200",
            "false,true,200",
            "true,false,403"
    })
    void testTicketValidationDisabled(String ticketDisabled, String tvrDisabled, Integer expectedStatusCode){
        JSONObject ticketRecord = getTicketRequest_extended(null, LocalDateTime.now().minusMinutes(30).format(formatter),
                LocalDate.now().minusYears(35).toString(),  "1h",ticketDisabled, "false", "C", "false");
        Long ticketId = addTicket(ticketRecord);
        JSONObject tvrRecord = getTicketValidationRequest(ticketId.toString(), "A", LocalDateTime.now().toString(), tvrDisabled, null, "false");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(tvrRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(expectedStatusCode);
    }

    @Test
    void testTicketValidationInvalidTicketId(){
        JSONObject ticketRecord = getTicketRequest_extended(null, LocalDateTime.now().minusMinutes(30).format(formatter),
                LocalDate.now().minusYears(35).toString(),  "1h","false", "false", "C", "false");
        long invalidTicketId = ThreadLocalRandom.current().nextLong();
        JSONObject tvrRecord = getTicketValidationRequest(Long.toString(invalidTicketId), "A", LocalDateTime.now().toString(), "false", null, "false");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(tvrRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(403);

    }

    @Test
    void testTicketValidationInvalidDiscountCardId(){
        Long customerId = getValidCustomerIdWithBirthdate(LocalDate.now().minusYears(35));
        JSONObject discountCardRecord = getDiscountCard(customerId.toString(),"25",LocalDate.now().minusDays(20).toString(),"30d");
        long invalidDiscountCardId = ThreadLocalRandom.current().nextLong();

        JSONObject ticketRecord = getTicketRequest_extended(null, LocalDateTime.now().minusMinutes(30).format(formatter),
                LocalDate.now().minusYears(35).toString(),  "1h","false", "true", "C", "false");
        Long ticketId = addTicket(ticketRecord);
        JSONObject tvrRecord = getTicketValidationRequest(ticketId.toString(), "A", LocalDateTime.now().toString(),
                "false", Long.toString(invalidDiscountCardId),"false");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(tvrRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(403);
    }

    @ParameterizedTest(name="Ticket validFrom {0} minutes ago which is valid for {1} → validation should return {2}")
    @CsvSource({
            "30,1h,200",
            "65,1h,403",
            "1200,1d,200",
            "1600,1d,403",
            "10000,30d,200,",
            "45000,30d,403",
            "0,1h,200",
            "59,1h,200",
            "60,1h,200"
    })
    void testTicketValidationTicketTimeframe(Integer minsToSubstract, String validFor, Integer expetedStatusCode){
        JSONObject ticketRecord = getTicketRequest_extended(null, LocalDateTime.now().minusMinutes(minsToSubstract).format(formatter),
                LocalDate.now().minusYears(35).toString(),  validFor,"false", "false", "C", "false");
        Long ticketId = addTicket(ticketRecord);
        JSONObject tvrRecord = getTicketValidationRequest(ticketId.toString(), "A", LocalDateTime.now().toString(), "false", null, "false");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(tvrRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(expetedStatusCode);
    }

    @ParameterizedTest(name="DiscountCard validFrom {0} days ago which is valid for {1} and Ticket bought 30 minutes ago → validation should return {2}")
    @CsvSource({
            "20,30d,200",
            "40,30d,403",
            "20,1y,200",
            "400,1y,403",
            "30,30d,200",
            "0,30d,200"
    })
    void testTicketValidationDiscountCardTimeframe(Integer daysToSubstract, String validFor, Integer expetedStatusCode){
        Long customerId = getValidCustomerIdWithBirthdate(LocalDate.now().minusYears(35));
        JSONObject discountCardRecord = getDiscountCard(customerId.toString(),"25",LocalDate.now().minusDays(daysToSubstract).toString(),validFor);
        Long discountCardId = addDiscountCard(discountCardRecord);

        JSONObject ticketRecord = getTicketRequest_extended(null, LocalDateTime.now().minusMinutes(30).format(formatter),
                LocalDate.now().minusYears(35).toString(),  "1h","false", "true", "C", "false");
        Long ticketId = addTicket(ticketRecord);
        JSONObject tvrRecord = getTicketValidationRequest(ticketId.toString(), "A", LocalDateTime.now().toString(),
                "false", discountCardId.toString(),"false");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(tvrRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(expetedStatusCode);
    }

    @ParameterizedTest(name="Customer birthdate is {0}, Ticket birthdate is {1} → expected validation result {2}")
    @CsvSource({
            "1980-01-01,1980-01-01,200",
            "1980-01-01,1980-01-02,403",
            "2000-02-28,2000-02-28,200",
            "2000-02-28,2000-02-27,403",
    })
    void testTvrBirthdateWithCustomerIncluded(String cBirthdate, String ticketBirthdate, Integer expetedStatusCode){
        LocalDate customerBirthdate = LocalDate.parse(cBirthdate);
        Long customerId = getValidCustomerIdWithBirthdate(customerBirthdate);
        LocalDateTime dateTime = LocalDateTime.now();
        Boolean disabled = false;
        String zone = "C";
        Boolean student = false;
        Long discountCardId = getValidDiscountCardId(customerId.toString(), "25", dateTime.minusDays(5).toLocalDate().toString(), "1y");
        Long ticketId = getValidTicketId(dateTime.toString(),ticketBirthdate,"30d", disabled,true,zone,false);
        JSONObject tvrRecord = getTicketValidationRequest(ticketId.toString(), zone, LocalDateTime.now().toString(),
                disabled.toString(), discountCardId.toString(),student.toString());
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(tvrRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(expetedStatusCode);
    }

    @ParameterizedTest(name="Customer(disabled)={0}, Ticket(disabled)={1}, TicketValidationRequest(disabled)={2} → expected validation result {3}")
    @CsvSource({
            "false,true,true,403",
            "false,false,true,200",
            "false,false,false,200",
            "true,false,true,200",
            "true,true,false,403",
            "true,false,false,200",
            "true,true,true,200",
            "false,true,false,403",
    })
    void testTvrDisabledWithCustomerIncluded(Boolean disCustomer, Boolean disTicket, Boolean disTVR, Integer expetedStatusCode){
        LocalDate customerBirthdate = LocalDate.now().minusYears(35);
        Long customerId = getValidCustomerId(customerBirthdate, disCustomer);
        LocalDateTime dateTime = LocalDateTime.now();
        String zone = "C";
        Boolean student = false;
        Long discountCardId = getValidDiscountCardId(customerId.toString(), "25", dateTime.minusDays(5).toLocalDate().toString(), "1y");
        Long ticketId = getValidTicketId(dateTime.toString(),customerBirthdate.toString(),"30d", disTicket,true,zone,false);
        JSONObject tvrRecord = getTicketValidationRequest(ticketId.toString(), zone, LocalDateTime.now().toString(),
                disTVR.toString(), discountCardId.toString(),student.toString());
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(tvrRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(expetedStatusCode);
    }

    @ParameterizedTest
    @CsvSource({
            "false,true,200",
            "false,false,200",
            "true,false,403",
            "true,true,200",
    })
    void testTvrStudentInterrelationWithTicket(Boolean ticketStudent, Boolean tvrStudent, Integer expetedStatusCode){
        LocalDate birthdate = LocalDate.now().minusYears(20);
        LocalDateTime dateTime = LocalDateTime.now();
        String zone = "C";
        Long ticketId = getValidTicketId(dateTime.toString(),birthdate.toString(),"30d", false,false,zone,ticketStudent);
        JSONObject tvrRecord = getTicketValidationRequest(ticketId.toString(), zone, LocalDateTime.now().toString(),
                "false", null,tvrStudent.toString());
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(tvrRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(expetedStatusCode);
    }

    @Test
    void testTvrDiscountCardBookedButNotProvided(){
        LocalDate birthdate = LocalDate.now().minusYears(20);
        LocalDateTime dateTime = LocalDateTime.now();
        String zone = "C";
        Long ticketId = getValidTicketId(dateTime.toString(),birthdate.toString(),"30d", false,true,zone,false);
        JSONObject tvrRecord = getTicketValidationRequest(ticketId.toString(), zone, LocalDateTime.now().toString(),
                "false", null, "false");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(tvrRecord.toString())
                .post(VALIDATE_TICKET_PATH)
                .then()
                .statusCode(403);
    }



    static JSONObject getTicketValidationRequest(String ticketId, String zone, String date, String disabled, String discountCardId, String student){
        JSONObject ticketRequestData = new JSONObject();
        try {
            ticketRequestData.put("ticketId", ticketId);
            ticketRequestData.put("zone", zone);
            ticketRequestData.put("date", date);
            ticketRequestData.put("disabled", disabled);
            ticketRequestData.put("discountCardId", discountCardId);
            ticketRequestData.put("student", student);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return ticketRequestData;
    }

    Long addTicket(JSONObject ticketRecord){
        return given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(ticketRecord.toString())
                .post(ADD_TICKET_PATH)
                .then()
                .statusCode(201)
                .extract().jsonPath().get("id");
    }

    Long getValidCustomerId(){
        return getValidCustomerId(LocalDate.parse("2020-01-01"), false);
    }

    Long getValidCustomerIdWithBirthdate(LocalDate birthdate){
        return getValidCustomerId(birthdate, false);
    }

    Long getValidCustomerId(LocalDate birthdate, Boolean disabled){
        JSONObject customerRecord = getCustomer(birthdate.toString(), disabled.toString());
        return addCustomer(customerRecord);
    }

    JSONObject getCustomer(String birthdate, String disabled){
        JSONObject customerData = new JSONObject();
        try {
            customerData.put("birthdate", birthdate);
            customerData.put("disabled", disabled);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return customerData;
    }

    Long addCustomer(JSONObject customerRecord){
        return given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(customerRecord.toString())
                .post(ADD_CUSTOMER_PATH)
                .then()
                .statusCode(201)
                .extract().jsonPath().get("id");
    }

    JSONObject getDiscountCard(String customerId, String type, String validFrom, String validFor){
        JSONObject discountCardData = new JSONObject();
        try {
            discountCardData.put("customerId", customerId);
            discountCardData.put("type", type);
            discountCardData.put("validFrom", validFrom);
            discountCardData.put("validFor", validFor);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return discountCardData;
    }

    Long getValidDiscountCardId(String customerId, String type, String validFrom, String validFor){
        JSONObject discountCardRecord = getDiscountCard(customerId, type, validFrom, validFor);
        return addDiscountCard(discountCardRecord);
    }

    Long getValidTicketId(String validFrom, String birthdate, String validFor, Boolean disabled, Boolean discountCard, String zone, Boolean student){
        JSONObject trRecord = getTicketRequest_extended(null, validFrom, birthdate, validFor, disabled.toString(), discountCard.toString(), zone, student.toString());
        return addTicket(trRecord);
    }

    String getDiscountCardPath(String customerId){
        return String.format(DISCOUNTCARD_PATH, customerId);
    }

    Long addDiscountCard(JSONObject discountCardRecord){
        try {
            String customerId = discountCardRecord.get("customerId").toString();
            return given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                    .body(discountCardRecord.toString())
                    .post(getDiscountCardPath(customerId))
                    .then()
                    .statusCode(201)
                    .extract().jsonPath().get("id");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Tests for change log
     */
    @ParameterizedTest
    @CsvSource({
            "1940-01-01, 1d",
            "2020-12-01, 30d",
            "1968-03-13, 1y"
    })
    void testValidFromRequired(String birthdate, String validFor){
        JSONObject ticketRequestData = getTicketRequest_noValidFrom(birthdate,validFor);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(ticketRequestData.toString())
                .post(ADD_TICKET_PATH)
                .then()
                .statusCode(500);
    }
    JSONObject getTicketRequest_noValidFrom(String birthdate, String validFor){
        JSONObject ticketRequestData = new JSONObject();
        try {
            ticketRequestData.put("birthdate", birthdate);
            ticketRequestData.put("validFor", validFor);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return ticketRequestData;
    }
}