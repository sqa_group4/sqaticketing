package de.rwth.swc.sqa.api;

import io.restassured.RestAssured;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class CustomerControllerTest {

    static final String ADD_CUSTOMER_PATH = "/customers";
    static final String DISCOUNTCARD_PATH = ADD_CUSTOMER_PATH + "/%s/discountcards";
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String CONTENT_TYPE = "application/json";
    @LocalServerPort
    private int port;

    @BeforeEach
    void setUp() {
        RestAssured.port = port;
    }

    static String getDiscountCardPath(Long customerId){
        return getDiscountCardPath(customerId.toString());
    }

    static String getDiscountCardPath(String customerId){
        return String.format(DISCOUNTCARD_PATH, customerId);
    }

    @ParameterizedTest
    @CsvSource({
            ",2020-01-01, ",
            ",2020-12-01, True",
            ",2020-01-01, False"
    })
    void addCustomer(String id, String birthdate, Boolean disabled) {
        JSONObject customerData = getCustomer(id, birthdate, String.valueOf(disabled));
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(customerData.toString())
                .post(ADD_CUSTOMER_PATH)
                .then()
                .statusCode(201)
                .body("id", not(nullValue()))
                .body("disabled", equalTo(disabled != null && disabled))
                .body("birthdate", equalTo(birthdate));
    }

    @Test
    void addUnbornCustomer(){
        JSONObject customerData = getCustomer(null, LocalDate.now().plusDays(30).toString(), null);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(customerData.toString())
                .post(ADD_CUSTOMER_PATH)
                .then()
                .statusCode(400);
    }

    @Test
    void addCustomerOnBirthdate(){
        JSONObject customerData = getCustomer(null, LocalDate.now().toString(), null);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(customerData.toString())
                .post(ADD_CUSTOMER_PATH)
                .then()
                .statusCode(201);
    }

    @ParameterizedTest
    @CsvSource({
            "1,2020-01-01,",
            "Q1,2020-01-01,",
            ",invalid date, ",
            ",,",
            ",2020-13-33,",
            ",2020-01-01,disabled"
    })
    void testInvalidInputsOnAddCustomer(String id, String birthdate, String disabled){
        JSONObject customerData = getCustomer(id, birthdate, disabled);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(customerData.toString())
                .post(ADD_CUSTOMER_PATH)
                .then()
                .statusCode(400);
    }

    @Test
    void testAddingDiscountCardsWithInvalidCustomerId(){
        Long validCustomerId = getValidCustomerId();
        Long invalidCutomerId = ThreadLocalRandom.current().nextLong();
        JSONObject discountCardData = getDiscountCard(invalidCutomerId.toString(), "25", "2020-01-01", "30d");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(discountCardData.toString())
                .post(getDiscountCardPath(invalidCutomerId))
                .then()
                .statusCode(404);
        // test validCustomerId in url invalid in data
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(discountCardData.toString())
                .post(getDiscountCardPath(validCustomerId))
                .then()
                .statusCode(400);
    }

    @ParameterizedTest(name = "testing for {3}")
    @CsvSource({
            "15,2020-01-01,30d, invalid type",
            "25,2020-02-30,30d, invalid date in validFrom",
            "50,2020-01-01,5d,  validFor value not in enum",
            "50,2020-01-01,5y,  validFor value not in enum"
    })
    void testAddingDiscountCardsWithInvalidInput(String type, String validFrom, String validFor, String desc){
        Long validCustomerId = getValidCustomerId();
        JSONObject discountCardData = getDiscountCard(validCustomerId.toString(), type, validFrom, validFor);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(discountCardData.toString())
                .post(getDiscountCardPath(validCustomerId))
                .then()
                .statusCode(400);
    }

    @Test
    void testAddingDiscountCardsWithOverlappingTimeframes(){
        Long validCustomerId = getValidCustomerId();
        Long validCustomerId2 = getValidCustomerId();
        JSONObject discountCard = getDiscountCard(validCustomerId.toString(), "25", "2020-01-01", "30d");
        JSONObject overlappingDiscountCard = getDiscountCard(validCustomerId.toString(), "25", "2020-01-15", "30d");
        JSONObject discountCardOfCustomer2 = getDiscountCard(validCustomerId2.toString(), "25", "2020-01-15", "30d");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(discountCard.toString())
                .post(getDiscountCardPath(validCustomerId))
                .then()
                .statusCode(201);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(overlappingDiscountCard.toString())
                .post(getDiscountCardPath(validCustomerId))
                .then()
                .statusCode(409);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(discountCardOfCustomer2.toString())
                .post(getDiscountCardPath(validCustomerId2))
                .then()
                .statusCode(201);
    }

    @ParameterizedTest
    @CsvSource({
            "2020-01-01",
            "2020-02-25",
            "2020-12-12",
    })
    void testAddingDiscountCards(String validFrom){
        Long customerId = getValidCustomerId();
        LocalDate start = LocalDate.parse(validFrom);
        final String[] validForValues = {"1y", "30d"};
        final Integer[] typeValues = new Integer[]{25, 50};
        for (int i=0; i<10; i++){
            String validFor = validForValues[ThreadLocalRandom.current().nextInt(validForValues.length)];
            Integer type = typeValues[ThreadLocalRandom.current().nextInt(typeValues.length)];
            JSONObject discountCardData = getDiscountCard(customerId.toString(), type.toString(), start.toString(), validFor);
            given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                    .body(discountCardData.toString())
                    .post(getDiscountCardPath(customerId))
                    .then()
                    .statusCode(201)
                    .body("customerId", equalTo(customerId))
                    .body("type", equalTo(type))
                    .body("validFrom", equalTo(start.toString()))
                    .body("validFor", equalTo(validFor));
            start = getTimeframeEnd(start, validFor);
        }
    }

    /**
     * Add the time interval given in validFor to the given date and return the resulting end date
     * @param beginTimeframe start date of the timeframe
     * @param validFor time interval of the timeframe. Must be in ["30d", "1y"]
     * @return end date of the timeframe
     */
    static LocalDate getTimeframeEnd(LocalDate beginTimeframe, String validFor){
        return switch (validFor){
            case "1y" -> beginTimeframe.plusYears(1);
            case "30d" -> beginTimeframe.plusDays(30);
            default -> beginTimeframe;
        };
    }

    @Test
    void testAddingDiscountCardBefore(){
        Long customerId = getValidCustomerId();
        LocalDate start = LocalDate.now();
        LocalDate before = start.minusDays(30);

        JSONObject discountCardData = getDiscountCard(customerId.toString(), "50", start.toString(), "1y");
        JSONObject discountCardDataBefore = getDiscountCard(customerId.toString(), "50", before.toString(), "30d");
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(discountCardData.toString())
                .post(getDiscountCardPath(customerId))
                .then()
                .statusCode(201);
        given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body(discountCardDataBefore.toString())
                .post(getDiscountCardPath(customerId))
                .then()
                .statusCode(201);
    }

    @Test
    void testGettingDiscountCards(){
        Long customerId = getValidCustomerId();
        LocalDate start = LocalDate.now();
        final String[] validForValues = {"1y", "30d"};
        final Integer[] typeValues = new Integer[]{25, 50};
        List<Long> expectedDiscountCards = new ArrayList<>();
        for (int i=0; i<10; i++){
            String validFor = validForValues[ThreadLocalRandom.current().nextInt(validForValues.length)];
            Integer type = typeValues[ThreadLocalRandom.current().nextInt(typeValues.length)];
            JSONObject discountCardData = getDiscountCard(customerId.toString(), type.toString(), start.toString(), validFor);
            Long discountCardId = given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                    .body(discountCardData.toString())
                    .post(getDiscountCardPath(customerId))
                    .then()
                    .statusCode(201)
                    .body("customerId", equalTo(customerId))
                    .body("type", equalTo(type))
                    .body("validFrom", equalTo(start.toString()))
                    .body("validFor", equalTo(validFor))
                    .extract().jsonPath().get("id");
            expectedDiscountCards.add(discountCardId);
            start = getTimeframeEnd(start, validFor);
        }
        final List<Map<String, Object>> response = given().get(getDiscountCardPath(customerId))
                .then()
                .statusCode(200)
                .body("$", iterableWithSize(expectedDiscountCards.size()))
                .extract().jsonPath().getList("$");
        response.stream().forEach(stringObjectMap -> {
            Object id = stringObjectMap.get("id");
            if (id instanceof Long){
                Long i = (Long) id;
                assertTrue(expectedDiscountCards.contains(i));
            }

        });

    }

    @Test
    void testGettingDiscountCard404(){
        Long validCustomerId = getValidCustomerId();
        given().get(getDiscountCardPath(validCustomerId))
                .then()
                .statusCode(404);
        Long unkownCustomerId = ThreadLocalRandom.current().nextLong();
        given().get(getDiscountCardPath(unkownCustomerId))
                .then()
                .statusCode(404);
    }

    @ParameterizedTest
    @CsvSource({
            "15854854548L",
            "2020-02-25",
            "id",
    })
    void testGettingDiscountCard400(String customerId){
        given().get(getDiscountCardPath(customerId))
                .then()
                .statusCode(400);
    }

    Long getValidCustomerId(){
        return given().header(CONTENT_TYPE_HEADER, CONTENT_TYPE)
                .body("{\"birthdate\": \"2020-01-01\"}")
                .post(ADD_CUSTOMER_PATH)
                .then()
                .statusCode(201)
                .extract().jsonPath().get("id");
    }

    JSONObject getDiscountCard(String customerId, String type, String validFrom, String validFor){
        JSONObject discountCardData = new JSONObject();
        try {
            discountCardData.put("customerId", customerId);
            discountCardData.put("type", type);
            discountCardData.put("validFrom", validFrom);
            discountCardData.put("validFor", validFor);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return discountCardData;
    }

    JSONObject getCustomer(String id, String birthdate, String disabled){
        JSONObject customerData = new JSONObject();
        try {
            customerData.put("id", id);
            customerData.put("birthdate", birthdate);
            customerData.put("disabled", disabled);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return customerData;
    }
}